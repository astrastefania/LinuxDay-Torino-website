<?php
# Linux Day Torino content management system
# Copyright (C) 2016-2023 Valerio Bozzolan, Linux Day Torino website contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

/*
 * Conference editor.
 *
 * From this page you can create/edit an Conference.
 * You can change only basic fields at the moment.
 */

// load configurations and framework
require 'load.php';

// Check if you can edit all Conferences.
// TODO: In the future this may be more modular, like for Events.
require_permission( 'edit-conferences' );

// Specify what Conference.
$conference_uid = null;
if( isset( $_GET['uid'] ) ) {
	$conference_uid = $_GET['uid'];
}

// Get the Conference.
$conference = null;
if( $conference_uid ) {

	// Check if the Conference exists
	$conference = FullConference::factoryFromUID( $conference_uid )
		->joinLocation()
		->queryRow();

	if( !$conference ) {
		die_with_404();
	}
}

$warning = null;

if( is_action( 'save-conference' ) ) {

	$data = [];
	$data[] = new DBCol( Conference::TITLE,       $_POST['title'],       's' );
	$data[] = new DBCol( Conference::UID,         $_POST['uid'],         's' );
	$data[] = new DBCol( Conference::START,       $_POST['start'],       's' );
	$data[] = new DBCol( Conference::END,         $_POST['end'],         's' );
	$data[] = new DBCol( Conference::DAYS,        $_POST['days'],        'd' );
	$data[] = new DBCol( Location::ID,            $_POST['location'],    'd' );
	$data[] = new DBCol( Conference::EVENTS_URL,  $_POST['events_url'],  'snull' );
	$data[] = new DBCol( Conference::PERSONS_URL, $_POST['persons_url'], 'snull' );

	// convert empty strings to NULL, if possible
	foreach( $data as $row ) {
		$row->promoteNULL();
	}

	if( $conference ) {
		// update the existing Conference
		( new QueryConference() )
			->whereConference( $conference )
			->update( $data );
	} else {
		// insert a new Conference
		Conference::factory()
			->insertRow( $data );
	}

	$id = $conference ? $conference->getConferenceID() : last_inserted_ID();

	// get the updated Conference
	$conference = FullConference::factory()
		->whereInt( Conference::ID, $id )
		->queryRow();

	// POST-redirect-GET
	http_redirect( $conference->getConferenceEditURL(), 303 );
}

if( $conference ) {
	Header::spawn( null, [
		'title' => sprintf(
			__("Modifica %s: %s"),
			__( "Conferenza" ),
			$conference->getConferenceTitle()
		),
	] );
} else {
	Header::spawn( null, [
		'title' => sprintf(
			__( "Aggiungi %s" ),
			__( "Conferenza" )
		),
	] );
}

// Get all known Locations that could be associated to this Conference.
$locations = Location::factory()
	->select( [
		Location::ID,
		Location::NAME,
	] )
	->orderBy( Location::NAME )
	->queryGenerator();
?>

	<?php if( $warning ): ?>
		<div class="card-panel yellow"><?= esc_html( $warning ) ?></div>
	<?php endif ?>

	<?php if( $conference ): ?>
		<p><?= HTML::a(
			ADMIN_BASE . "/conferences.php",
			__( "Conferenze" ) . icon( 'home', 'left' )
		) ?></p>

		<p><?= HTML::a(
			// href
			$conference->getConferenceURL(),

			// text
			__( "Vedi" ) . icon( 'account_box', 'left' )
		) ?></p>
	<?php endif ?>

	<form method="post">
		<?php form_action( 'save-conference' ) ?>

		<div class="row">

			<div class="col s12 m4 l3">
				<div class="card-panel">
					<label for="conference-title"><?= __( "Titolo" ) ?></label>
					<input  id="conference-title" type="text" name="title" required<?php
						if( $conference ) {
							echo value( $conference->get( Conference::TITLE ) );
						}
					?> />
				</div>
			</div>

			<div class="col s12 m4 l3">
				<div class="card-panel">
					<label for="conference-uid"><?= __( "Codice" ) ?></label>
					<input  id="conference-uid" type="text" name="uid" required<?php
						if( $conference ) {
							echo value( $conference->get( Conference::UID ) );

							// This avoids stupid mistakes on permalinks.
							// Indeed if you know what you are doing, this should be editable.
							echo " readonly";
						}
					?> />
				</div>
			</div>

		</div>


		<div class="row">

			<div class="col s12 m4 l3">
				<div class="card-panel">
					<label for="conference-start"><?= __( "Inizio" ) ?></label>
					<input  id="conference-start" type="text" name="start" required placeholder="Y-m-d H:i:s"<?php
						if( $conference ) {
							echo value( $conference->getConferenceStart()->format( 'Y-m-d H:i:s' ) );
						} elseif( isset( $_GET['start'] ) ) {
							echo value( $_GET['start'] );
						}
					?> />
				</div>
			</div>

			<div class="col s12 m4 l3">
				<div class="card-panel">
					<label for="conference-end"><?= __( "Fine" ) ?></label>
					<input  id="conference-end" type="text" name="end" required placeholder="Y-m-d H:i:s"<?php
						if( $conference ) {
							echo value( $conference->getConferenceEnd()->format( 'Y-m-d H:i:s' ) );
						} elseif( isset( $_GET['end'] ) ) {
							echo value( $_GET['end'] );
						}
					?> />
				</div>
			</div>

			<div class="col s12 m4 l3">
				<div class="card-panel">
					<label for="conference-days"><?= __( "Giorni" ) ?></label>
					<input  id="conference-days" type="number" name="days" required<?php
						if( $conference ) {
							echo value( $conference->getConferenceDays() );
						} elseif( isset( $_GET['days'] ) ) {
							echo value( $_GET['days'] );
						}
					?> />
				</div>
			</div>

		</div>

		<div class="row">

			<div class="col s12 m6">
				<div class="card-panel">
					<label for="conference-events-url"><?= __( "URL Eventi" ) ?></label>
					<input  id="conference-events-url" type="text" name="events_url" <?php
						if( $conference ) {
							echo value( $conference->get( Conference::EVENTS_URL ) );
						}
					?> />
				</div>
			</div>

			<div class="col s12 m6">
				<div class="card-panel">
					<label for="conference-persons-url"><?= __( "URL Relatori" ) ?></label>
					<input  id="conference-persons-url" type="text" name="persons_url" <?php
						if( $conference ) {
							echo value( $conference->get( Conference::PERSONS_URL ) );
						}
					?> />
				</div>
			</div>

		</div>

		<div class="row">

			<div class="col s12 m4 l3">
				<div class="card-panel">
					<label for="location"><?= __( "Sede" ) ?></label>
					<select id="location" name="location">
						<?php
							foreach( $locations as $location ) {

								$option = ( new HTML( 'option' ) )
									->setText( $location->getLocationName() )
									->setAttr( 'value', $location->getLocationID() );

								if( $location->getLocationID() === $conference->getLocationID() ) {
									$option->setAttr( 'selected', 'selected' );
								}

								echo $option->render();
							}

						?>
					</select>

				</div>
			</div>

		</div>

		<!-- Save Start -->
		<div class="row">
			<div class="col s12">
				<button type="submit" class="btn waves-effect purple"><?= __( "Salva" ) . icon( 'save', 'left' ) ?></button>
			</div>
		</div>
		<!-- Save End -->

	</form>

<?php

Footer::spawn();
