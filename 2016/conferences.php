<?php
# Linux Day Torino website
# Copyright (C) 2016-2023 Valerio Bozzolan, Linux Day Torino contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'load.php';

require_permission( 'edit-conferences' );

Header::spawn( null, [
	'title' => __( "Conferenze" ),
] );

// check if the Conference exists
$conferences = ( new QueryConference() )
	->orderBy( Conference::END, 'desc' )
	->queryGenerator();

?>

	<div class="row">
		<div class="col s12">
			<a class="btn waves-effect purple" href="<?= esc_attr( Conference::editURL( [ 'new' => 1 ] ) ) ?>"><?= __( "Crea" ) . icon( 'save', 'left' ) ?></a>
		</div>
	</div>

	<div class="row">
	<?php foreach( $conferences as $conference ): ?>
		<div class="col s12">
			<div class="card-panel">
				<p>
					<h2><a href="<?= esc_attr( $conference->getConferenceURL() ) ?>"><?= esc_html( $conference->getConferenceTitle() ) ?></a></h2>

					<!-- start edit button -->
					<a class="btn waves-effect purple" href="<?= esc_attr( $conference->getConferenceEditURL() ) ?>"><?= __( "Modifica" ) . icon( 'save', 'left' ) ?></a>
					<!-- stop edit button -->

					<!-- start edit button -->
					<a class="btn waves-effect purple" href="<?= esc_attr( $conference->getConferenceEventsEditURL() ) ?>"><?= __( "Eventi" ) . icon( 'format_list_bulleted', 'left' ) ?></a>
					<!-- stop edit button -->
				</p>
				<hr />
				<p>
					<?= esc_html( $conference->getConferenceHumanStart() ) ?>
				</p>
			</div>
		</div>
	<?php endforeach ?>
	</div>

<?php

Footer::spawn();
